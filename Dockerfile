# build stage
FROM golang:alpine AS build-env
RUN apk add --no-cache git
RUN go get -u github.com/boyter/scc/

FROM alpine:3.12
RUN apk add --no-cache git
WORKDIR /app
COPY --from=build-env /go/bin/scc /usr/bin/scc
CMD ["scc"]