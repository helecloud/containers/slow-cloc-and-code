# Slow Cloc and Code Container

Slow Cloc and Code: scc is a very fast accurate code counter with complexity calculations and COCOMO estimates written in pure Go (https://github.com/boyter/scc). This is a Docker container for using inside CI.

# Maintainer

- Will Hall
